import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class AltaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div id="blocker">
      </div>

      <div class="row">
          <div class="shadow-lg col-4 offset-4 container container-comun">
  			<h2>Bienvenido a Bank of Moderdonia</h2>
  			<div class="container-fluid">
  				<div class="row">
  					<div class="offset-4">
  						<img src="src/pagina-principal/fotos/logo-2.png">
  					</div>
  				</div>
              </div>
        	    </br>
              <div class="card card-alta">
  				</br>
  				<span>Por favor rellene sus datos:</span>
  				</br>
  					<form name="datosalta" action="#">
  						<div class="form-group">
  							<input id="nombre" type="text" class="form-control"  placeholder="* Nombre" value="{{nombre::input}}" on-change="validaCompleto" autofocus>
  						</div>
  						<div class="form-group">
  							<input id="apellido" type="text" class="form-control" placeholder="* Apellidos" value="{{apellido::input}}" on-change="validaCompleto">
  						</div>
  						<div class="form-group">
  							<input id="domicilio" type="text" class="form-control" placeholder="Domicilio" value="{{domicilio::input}}">
  						</div>
  						<div class="form-inline">
  							<input id="provincia" type="text" class="form-control mb-3 mr-sm-3" placeholder="Provincia" value="{{provincia::input}}">
    				    <input id="codpostal" type="text" class="form-control mb-3 mr-sm-3" placeholder="* Código Postal" value="{{codpostal::input}}" on-change="validaCompleto">
  						</div>
  						<div class="form-inline">
  							<input id="nif" type="text" class="form-control mb-3 mr-sm-3" placeholder="* NIF" value="{{nif::input}}" on-change="validaCompleto">
  							<input id="telefono" type="text" class="form-control mb-3 mr-sm-3" placeholder="* Número de Teléfono" value="{{telefono::input}}" on-change="validaCompleto">
  						</div>
              <div class="form-inline">
  							<input id="email" type="email" class="form-control mb-3 mr-sm-3" placeholder="* Email" value="{{email::input}}" on-change="validaCompleto">
  						</div>
              <div class="form-inline">
  							<input id="password" type="password" class="form-control mb-3 mr-sm-3" placeholder="* Password" value="{{password::input}}" on-change="validaCompleto">
  							<input id="password2" type="password" class="form-control mb-3 mr-sm-3" placeholder="* Confirme Password" value="{{password2::input}}" on-change="validaCompleto">
  						</div>
  					</form>
  			</div>
    		</br>
        <div id="cajaerror" class="alerterror">
            <span id="erroralta">ERROR:</span>
            </br>
        </div>
  			</br>
        <div class="text-center">
        		<button type="button" class="btn btn-secondary btn-alb" id="btnAlta" on-click="altaUsuario" disabled>Alta Usuario
              <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
            </button>
        		<button type="button" class="btn btn-secondary btn-alb" id="btnAltaCancel" on-click="cancelAlta">Cancelar</button>
        </div>
      </div>

      <iron-ajax
        id="doAlta"
        url="http://localhost:3000/apitechu/users/alta"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      nombre: {
        type: String
      }, apellido: {
        type: String
      }, domicilio: {
        type: String
      }, provincia: {
        type: String
      }, codpostal: {
        type: String
      }, nif: {
        type: String
      }, telefono: {
        type: String
      }, email: {
        type: String
      }, password: {
        type: String
      }, password2: {
        type: String
      }, idUsuario: {
        type: Number
      }
    };
  }

  altaUsuario() {
    console.log("Alta pulsado");

    if (this.password != this.password2){

      var elemento = this.$.erroralta;

      elemento.textContent = "ERROR: clave confirmada incorrecta"

      elemento = this.$.cajaerror;

      elemento.style.color = "black";
    }
    else {
      var altaData = {
        "nombre": this.nombre,
        "apellido": this.apellido,
        "domicilio": this.domicilio,
        "provincia": this.provincia,
        "codpostal": this.codpostal,
        "nif": this.nif,
        "telefono": this.telefono,
        "email": this.email,
        "password": this.password,
        "fecalta": new Date()
      }

      this.$.doAlta.body = JSON.stringify(altaData);
      this.$.doAlta.generateRequest();

      var elemento = this.$.spinner;

      elemento.hidden = false;
    }
  }

  cancelAlta() {
    console.log("Cancelar pulsado");

    this.generaEvento("cancelAlta");

    this.inicializa();
  }

  manageAJAXresponse(data) {

    if (data.detail.response.msg == "Usuario Creado") {

        this.idUsuario = data.detail.response.id;

        this.generaEvento("altaUsuario");

        this.inicializa();
    }
    else {
      var elemento = this.$.erroralta;

      elemento.textContent = data.detail.response.msg;

      elemento = this.$.cajaerror;

      elemento.style.color = "black";
    }

    var elemento = this.$.spinner;

    elemento.hidden = true;
  }

  showError(error) {

    var elemento = this.$.erroralta;

    if (error.detail.request.status == 404){

      elemento.textContent = "Usuario y/o password incorrectos";
    }
    else {
      elemento.textContent = error.detail.error.message;
    }

    elemento = this.$.spinner;

    elemento. hidden = true;

    elemento = this.$.cajaerror;

    elemento.style.color = "black";
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventalta",
        {
          detail: {
            "evento": evento,
            "idUsuario" : this.idUsuario,
            "email": this.email,
            "datos": {
              "nombre":this.nombre,
              "apellido": this.apellido,
              "domicilio": this.domicilio,
              "provincia": this.provincia,
              "codpostal": this.codpostal,
              "nif": this.nif,
              "telefono": this.telefono
            }
          }
        }
      )
    )
  }

  validaCompleto() {

    var elemento1 = this.$.nombre;
    var elemento2 = this.$.apellido;
    var elemento3 = this.$.codpostal;
    var elemento4 = this.$.nif;
    var elemento5 = this.$.telefono;
    var elemento6 = this.$.email;
    var elemento7 = this.$.password;

    if (!this.campoVacio(elemento1.value) && !this.campoVacio(elemento2.value) && !this.campoVacio(elemento3.value) &&
        !this.campoVacio(elemento4.value) && !this.campoVacio(elemento5.value) && !this.campoVacio(elemento6.value) &&
        !this.campoVacio(elemento7.value) && elemento7.value.length > 7) {

      console.log("Datos informados");

      var elemento = this.$.btnAlta;

      elemento.disabled = false;
      elemento.focus();
    }
  }

  campoVacio(datoEntrada) {

      var retorno = true;

      if (datoEntrada.length > 0 && datoEntrada != "")
      {
        for (var i = 0; i < datoEntrada.length; i++)
        {
          if (datoEntrada[i] != " ")
          {
            retorno = false;
          }
        }
      }

      return retorno;
  }

  inicializa() {

    var elemento = this.$.cajaerror;

    elemento.style.color = "#E72085";

    elemento = this.$.btnAlta;

    elemento.disabled = true;

    elemento = this.$.nombre;

    elemento.focus();

    this.nombre = "";
    this.apellido = "";
    this.domicilio = "";
    this.provincia = "";
    this.codpostal = "";
    this.nif = "";
    this.telefono = "";
    this.email = "";
    this.password = "";
    this.password2 = "";
    this.idUsuario = null;
  }
}

window.customElements.define('alta-usuario', AltaUsuario);
