import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../login-usuario/login-usuario.js';
import '../logout-usuario/logout-usuario.js';
import '../alta-usuario/alta-usuario.js';
import '../baja-usuario/baja-usuario.js';
import '../consulta-usuario/consulta-usuario.js';
import '../modifica-usuario/modifica-usuario.js';
import '../visor-cuentas/visor-cuentas.js';
import "@polymer/iron-pages/iron-pages.js";

/**
 * @customElement
 * @polymer
 */
class PaginaPrincipal extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          background-color: #E72085;
          width: 100%;

        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div class="card" id="top">
    		<div class="container-fluid">
    			<div class="row">
    				<div class="col-1 offset-2" id="bandera">
    					<img src="src/pagina-principal/fotos/bandera-moderdonia-pequeña-2.jpg">
    				</div>
    				<div class="col-9">
    					<div class="col-12">
    						<div class="row">
    							<div class="col-1 offset-3" id="logo">
    								<img src="src/pagina-principal/fotos/logo.png">
    							</div>
    							<div class="col-5 offset-3 btn-inicio">
    								<button type="button" id="btnalta" class="btn btn-secondary btn-alb" on-click="altaUsuario">Registrarse</button>
    								<button type="button" id="btnlogin" class="btn btn-secondary btn-alb" on-click="logInlogOut">Log In</button>
                    <div class="dropdown">
   								    <button type="button" id="btnusuario" class="btn btn-secondary dropdown-toggle btn-alb" on-click="opcionesUsuario" hidden></button>
    								    <div id="dropdownUsuario" class="dropdown-content">
    									    <a on-click="consultaUsuario" href="#">Consultar</a>
    									    <a on-click="editarUsuario" href="#">Editar</a>
    									    <a on-click="bajaUsuario" href="#">Dar de baja</a>
    								    </div>
                    </div>
    							</div>
    						</div>
    					</div>
    					<div class="col-12">
    						<nav class="navbar navbar-expand-sm navbar-light">
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">PARTICULARES</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">BANCA PERSONAL</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">BANCA PRIVADA</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">AUTONOMOS</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">EMPRESAS</a>
    								</li>
    							</ul>
    							<ul class="navbar-nav">
    								<li class="nav-item alb-item1">
    									<a class="nav-link" href="#">INSTITUCIONES</a>
    								</li>
    							</ul>
    						</nav>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>

    	<div class="card" id="menu">
    		<div class="container">
    			<div class="row">
    				<nav class="navbar navbar-expand navbar-dark offset-1">
    					<ul class="navbar-nav">
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-casa-24.png" on-click="volverPaginaPrincipal">
    						</div>
    						<li class="nav-item alb-item2">
    							<a id="home" class="nav-link" on-click="volverPaginaPrincipal">Home</a>
    						</li>
    						<div class="card card-icon">
    							<img id="navCuentas" src="src/pagina-principal/fotos/icons8-montón-de-dinero-24.png" on-click="opcionesCuentas">
    						</div>
                <li class="nav-item dropdown alb-item2" on-click="opcionesCuentas">
                  <a id="navCuentas" class="nav-link dropdown-toggle" data-toggle="dropdown">Cuentas</a>
                    <div id="dropdownCuentas" class="dropdown-content">
                      <a class="nav-link" on-click="altaCuenta" href="#">Contratar Cuenta</a>
                      <a class="nav-link" on-click="consultaCuentas" href="#">Consultar Cuentas</a>
                  </div>
                </li>
    						<div class="card card-icon">
    							<img src="src/pagina-principal/fotos/icons8-tarjetas-bancarias-filled-24.png">
    						</div>
    						<li class="nav-item alb-item2">
    							<a class="nav-link">Tarjetas</a>
    						</li>
    						<div class="card card-icon">
    							<img id="navPtmos" src="src/pagina-principal/fotos/icons8-hipoteca-filled-24.png" on-click="opcionesPtmos">
    						</div>
                <li class="nav-item dropdown alb-item2" on-click="opcionesPtmos">
                  <a id="navPtmos" class="nav-link dropdown-toggle" data-toggle="dropdown">Hipotecas y Préstamos</a>
                    <div id="dropdownPtmos" class="dropdown-content">
                      <a class="nav-link" on-click="consultaPtmos" href="#">Calcule su Préstamo</a>
                  </div>
                </li>
    						<li class="nav-item alb-item2">
    						</li>
    						<div class="card card-icon">
    							<img id="navAbout" src="src/pagina-principal/fotos/icons8-acerca-de-24.png" on-click="opcionesAbout">
    						</div>
    						<li class="nav-item dropdown alb-item2" on-click="opcionesAbout">
    							<a id="navAbout" class="nav-link dropdown-toggle" data-toggle="dropdown">About</a>
    								<div id="dropdownAbout" class="dropdown-content">
    									<a class="nav-link" on-click="quienesSomos">Quienes Somos</a>
    									<a class="nav-link" on-click="dondeEstamos">Donde Estamos</a>
    									<a class="nav-link" on-click="equipo">Nuestro Equipo</a>
    									<a class="nav-link" on-click="contacto">Contacto</a>
    								</div>
    						</li>
    					</ul>
    				</nav>
    			</div>
    		</div>
    	</div>

      <div id="fondo" class="container">
		    <img class="img-fluid mx-auto" src="src/pagina-principal/fotos/moderdonia2-930x600.jpg" width="100%" height="100%">
      </div>

      <iron-pages selected="[[componente]]" attr-for-selected="component-name">
        <div component-name="alta-usuario"><alta-usuario on-eventalta="procesaEvento"></alta-usuario></div>
        <div component-name="baja-usuario"><baja-usuario id="baja" on-eventbaja="procesaEvento"></baja-usuario></div>
        <div component-name="consulta-usuario"><consulta-usuario id="consulta" on-eventconsulta="procesaEvento"></consulta-usuario></div>
        <div component-name="modifica-usuario"><modifica-usuario id="modifica" on-eventmodifica="procesaEvento"></modifica-usuario></div>
        <div component-name="login-usuario"><login-usuario on-eventlogin="procesaEvento"></login-usuario></div>
        <div component-name="logout-usuario"><logout-usuario id="logout" on-eventlogout="procesaEvento"></logout-usuario></div>
        <div component-name="visor-cuentas"><visor-cuentas id="cuentas"></visor-cuentas></div>
      </iron-pages>

    `;
  }

  static get properties() {
    return {
      componente: {
        type: String
      }, idUsuario: {
        type: Number
      }, email: {
        type: String
      }, datos: {
        type: Object
      }, logged: {
        type: Boolean,
        value: false
      }
    };
  }

  ready() {

    this.addEventListener("click", this._onClick, true);

    super.ready();
  }

  _onClick(event) {

    this._ocultaDropdowns(event);
  }

  _ocultaDropdowns(e) {

    if (!e.composedPath()[0].matches("#btnusuario")){

      var elemento = this.$.dropdownUsuario;

      if (elemento.classList.contains("show")){

        elemento.classList.remove('show');
      }
    }

    if (!e.composedPath()[0].matches("#navCuentas")){

      var elemento = this.$.dropdownCuentas;

      if (elemento.classList.contains("show")){

        elemento.classList.remove('show');
      }
    }

    if (!e.composedPath()[0].matches("#navPtmos")){

      var elemento = this.$.dropdownPtmos;

      if (elemento.classList.contains("show")){

        elemento.classList.remove('show');
      }
    }

    if (!e.composedPath()[0].matches("#navAbout")){

      var elemento = this.$.dropdownAbout;

      if (elemento.classList.contains("show")){

        elemento.classList.remove('show');
      }
    }
  }

  opcionesUsuario() {

    var elemento = this.$.dropdownUsuario;

    elemento.classList.toggle("show");
  }

  opcionesCuentas() {

    if (this.logged){

      var elemento = this.$.dropdownCuentas;

      elemento.classList.toggle("show");
    }
  }

  opcionesPtmos() {

    var elemento = this.$.dropdownPtmos;

    elemento.classList.toggle("show");
  }

  opcionesAbout() {

    var elemento = this.$.dropdownAbout;

    elemento.classList.toggle("show");
  }

  bajaUsuario() {

    console.log("Baja usuario " + this.idUsuario + " " + this.email);

    this.$.baja.idUsuario = this.idUsuario;
    this.$.baja.email = this.email;
    this.componente = "baja-usuario";
  }

  editarUsuario() {

    console.log("Editar usuario");

    this.$.modifica.idUsuario = this.idUsuario;
    this.$.modifica.email = this.idUsuario;
    this.$.modifica.datos = this.datos;

    this.componente = "modifica-usuario";
  }

  consultaUsuario() {

    console.log("Consulta usuario");

    this.$.consulta.datos = this.datos;

    console.log("pasando nombre " + this.datos.name)
    this.componente = "consulta-usuario";
  }

  volverPaginaPrincipal() {

    console.log("Home pulsado");

    this.componente = "pagina-principal";
  }

  logInlogOut() {

    if (!this.logged){

      this.componente = "login-usuario";
    }
    else {
      this.$.logout.idUsuario = this.idUsuario;
      this.componente = "logout-usuario";
    }
  }

  altaUsuario() {

      this.componente = "alta-usuario";
  }

  consultaCuentas() {

    console.log("consulta cuentas");
    if (this.logged){

      var elemento = this.$.dropdownCuentas;

      elemento.classList.toggle("show");

      this.$.cuentas.idUsuario = this.idUsuario;

      elemento = this.$.fondo;

      elemento.hidden = true;

      this.componente = "visor-cuentas";
    }
  }

  procesaEvento(e) {
    console.log("Capturado evento del emisor");
    console.log(e.detail.evento);

    switch (e.detail.evento)
		{
	    case "logIn":

        this.usuarioConectado(e.detail.idUsuario, e.detail.datos, e.detail.email);

        break;

      case "logOut":

        this.usuarioDesconectado();

        break;

      case "altaUsuario":

        this.usuarioConectado(e.detail.idUsuario, e.detail.datos, e.detail.email);

        break;

      case "modificaUsuario":

        this.usuarioConectado(e.detail.idUsuario, e.detail.datos, e.detail.email);

        break;

      case "bajaUsuario":

        this.usuarioDesconectado();

        break;

      default:

       this.componente="pagina-principal";
    }
  }

  usuarioConectado(usuario, datos, email) {

    console.log("logando " + usuario + " " + datos.nombre);
    var elemento = this.$.btnlogin;

    elemento.textContent = "Log Out";

    elemento = this.$.btnusuario;

    elemento.textContent = datos.nombre;
    elemento.hidden = false;

    elemento = this.$.btnalta;
    elemento.disabled = true;

    this.logged = true;
    this.idUsuario = usuario;
    this.email = email;
    this.datos = datos;
    this.componente="pagina-principal";
  }

  usuarioDesconectado() {

    var elemento = this.$.btnlogin;

    elemento.textContent = "Log In";

    elemento = this.$.btnusuario;

    elemento.textContent = "";
    elemento.hidden = true;

    elemento = this.$.btnalta;
    elemento.disabled = false;

    this.logged = false;
    this.idUsuario = null;
    this.componente="pagina-principal";
  }
}

window.customElements.define('pagina-principal', PaginaPrincipal);
