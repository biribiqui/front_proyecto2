import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class BajaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div id="blocker">
      </div>

      <div class="shadow-lg card card-logout" id="baja">
    		<h4 class="card-title">BAJA DE USUARIO</h4>
    		</br>
        <div>Se va a dar del baja el usuario:
        </div>
  		  </br>
        <span style="font-weight:bold">{{email}}</span>
  		  </br>
        <div>¿Desea continuar?
        </div>
        </br>
        <div id="cajaerror" class="alertaerror">
            <span id="errorbaja">ERROR:</span>
            </br>
        </div>
  			</br>
  	 		<div class="text-center">
    			<button type="button" class="btn btn-secondary btn-formulario" id="btnAceptar" on-click="bajaUsuario">Aceptar
            <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
          </button>
    			<button type="button" class="btn btn-secondary btn-formulario" id="btnCancelar" on-click="cancelaBaja">Cancelar</button>
    		</div>
    	</div>

      <iron-ajax
        id="doBaja"
        url="http://localhost:3000/apitechu/users/baja/{{idUsuario}}"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      email: {
        type: String
      }, idUsuario: {
        type: Number
      }
    };
  }

  bajaUsuario() {
    console.log("baja pulsado " + this.idUsuario + " " + this.email);

    var bajaData = {
      "febaja": new Date()
    }

    this.$.doBaja.body = JSON.stringify(bajaData);
    this.$.doBaja.generateRequest();

    var elemento = this.$.spinner;

    elemento.hidden = false;
  }

  cancelaBaja() {
    console.log("Cancelar pulsado");
    var elemento = this.$.cajaerror;

    elemento.style.color = "#9F2B57";

    this.generaEvento("cancelBaja");
  }

  manageAJAXresponse(data) {

    if (data.detail.response.msg == "Baja correcta") {

        this.generaEvento("bajaUsuario");

        this.inicializa();
    }
    else {
      var elemento = this.$.errorbaja;

      elemento.textContent = data.detail.response.msg;

      elemento = this.$.cajaerror;

      elemento.style.color = "black";
    }

    var elemento = this.$.spinner;

    elemento.hidden = true;
  }

  showError(error) {

    var elemento = this.$.errorbaja;

    if (error.detail.request.status == 404){

      elemento.textContent = "Usuario incorrecto";
    }
    else {
      elemento.textContent = error.detail.error.message;
    }

    elemento = this.$.spinner;

    elemento. hidden = true;

    elemento = this.$.cajaerror;

    elemento.style.color = "black";
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventbaja",
        {
          detail: {
            "evento": evento,
            "idUsuario" : this.idUsuario
          }
        }
      )
    )
  }

  inicializa() {

    var elemento = this.$.cajaerror;

    elemento.style.color = "#9F2B57";
  }
}

window.customElements.define('baja-usuario', BajaUsuario);
