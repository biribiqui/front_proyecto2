import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div class="container" width="100%" height="100%">
  			<table class="table">
          <thead class="thead-light">
          <tr>
            <th>IBAN</th>
            <th>SALDO</th>
          </tr>
        </thead>
      	<tbody>
<!--          <dom-repeat items="{{cuentas}}"> -->
          <template is="dom-repeat" items="{{cuentas}}">
      	     <tr>
				         <td>[[item.iban]]</td>
						     <td>[[item.balance]]</td>
      		   </tr>
          </template>
<!--          </dom-repeat> -->
      	</tbody>
        </table>
      </div>

      <iron-ajax
        id="getAccounts"
        url="http://localhost:3000/apitechu/v2/accounts/{{idUsuario}}"
        handle-as="json"
        on-response="showData"
        on-error="trataError"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      cuentas: {
        type: Array
      }, idUsuario: {
        type: Number,
        observer: "_idUserChanged"
      }
    };
  }   //end properties

  _idUserChanged(newValue, oldValue) {
     console.log("lanzando petición");

     this.$.getAccounts.generateRequest();
   }

  showData(data) {
    console.log("showData");

    this.cuentas = data.detail.response;
  }

  trataError(error) {
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.error.message);

    this.cuentas = [];
  }
}

window.customElements.define('visor-cuentas', VisorCuentas);
