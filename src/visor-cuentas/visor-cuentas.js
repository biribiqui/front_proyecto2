import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="estilos.css" rel="stylesheet" type="text/css" />

      <div id="cabeceras" class="container container-cabecera">
        <div class="row">
          <div class="col-2 offset-2">IBAN</div>
          <div class="col-2 offset-6">SALDO</div>
        </div>
      </div>
<!--          <dom-repeat items="{{cuentas}}"> -->
      <template is="dom-repeat" items="{{cuentas}}">
        <div id="cuentas" class="container container-cuentas" on-click="clickCuenta">
          <div class="row">
            <div class="col-4">[[item.iban]]</div>
            <div class="col-2 offset-6">[[item.balance]]</div>
          </div>
        </div>
      </template>
<!--          </dom-repeat> -->
      </div>

      <iron-ajax
        id="getAccounts"
        url="http://localhost:3000/apitechu/v2/accounts/{{idUsuario}}"
        handle-as="json"
        on-response="showData"
        on-error="trataError"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      cuentas: {
        type: Array
      }, idUsuario: {
        type: Number,
        observer: "_idUserChanged"
      }
    };
  }   //end properties

  _idUserChanged(newValue, oldValue) {
     console.log("lanzando petición");

     this.$.getAccounts.generateRequest();
   }

  clickCuenta(e) {

    console.info("click cuenta " + e.model.item.iban);
  }

  showData(data) {
    console.log("showData");

    this.cuentas = data.detail.response;
  }

  trataError(error) {
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.error.message);

    this.cuentas = [];
  }
}

window.customElements.define('visor-cuentas', VisorCuentas);
