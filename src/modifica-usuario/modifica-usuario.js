import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class ModificaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link href="estilos.css" rel="stylesheet" type="text/css"/>

      <div id="blocker">
      </div>

      <div class="row">
          <div class="shadow-lg col-4 offset-4 container container-comun">
  			<h2>Bank of Moderdonia</h2>
  			<div class="container-fluid">
  				<div class="row">
  					<div class="offset-4">
  						<img src="src/pagina-principal/fotos/logo-2.png">
  					</div>
  				</div>
          </div>
          </br>
          <div class="card card-alta">
  				  </br>
  					<form name="datosconsulta" action="#">
  						<div class="form-group">
						    <label>* Nombre</label>
  							<input id="nombre" type="text" class="form-control" value="{{datos.nombre::input}}">
  						</div>
  						<div class="form-group">
						    <label>* Apellidos</label>
  							<input id="apellido" type="text" class="form-control" value="{{datos.apellido::input}}">
  						</div>
  						<div class="form-group">
						    <label>Domicilio</label>
  							<input id="domicilio" type="text" class="form-control" value="{{datos.domicilio::input}}">
  						</div>
						  <label>Provincia / * Código Postal</label>
  						<div class="form-inline">
  							<input id="provincia" type="text" class="form-control mb-3 mr-sm-3" value="{{datos.provincia::input}}">
    				    <input id="codpostal" type="text" class="form-control mb-3 mr-sm-3" value="{{datos.codpostal::input}}">
  						</div>
						  <label>* NIF / * Teléfono</label>
  						<div class="form-inline">
  							<input id="nif" type="text" class="form-control mb-3 mr-sm-3" value="{{datos.nif::input}}">
	 							<input id="telefono" type="text" class="form-control mb-3 mr-sm-3" value="{{datos.telefono::input}}">
  						</div>
  					</form>
  			</div>
    		</br>
        <div id="cajaerror" class="alerterror">
            <span id="errormodifica">ERROR:</span>
            </br>
        </div>
  			</br>
        <div class="text-center">
            <button type="button" class="btn btn-secondary btn-alb" id="btnModifica" on-click="modificaUsuario">Actualizar Usuario
              <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" hidden></span>
            </button>
        		<button type="button" class="btn btn-secondary btn-alb" id="btnCancelModifica" on-click="cancelaModifica">Cancelar</button>
        </div>
      </div>

      <iron-ajax
        id="doModifica"
        url="http://localhost:3000/apitechu/users/modifica/{{idUsuario}}"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>

    `;
  }

  static get properties() {
    return {
      datos: {
        type: Object
      }, idUsuario: {
        type: Number
      }, email: {
        type: String
      }
    };
  }

  cancelaModifica() {

    console.log("Cancelar pulsado");

    this.generaEvento("cancelaModifica");
  }

  modificaUsuario() {

    console.log("MOdificar pulsado");

    if (this.validaDatos()){

      var modificaData = {
        "nombre": this.datos.nombre,
        "apellido": this.datos.apellido,
        "domicilio": this.datos.domicilio,
        "provincia": this.datos.provincia,
        "codpostal": this.datos.codpostal,
        "nif": this.datos.nif,
        "telefono": this.datos.telefono
      }

      this.$.doModifica.body = JSON.stringify(modificaData);
      this.$.doModifica.generateRequest();

      var elemento = this.$.spinner;

      elemento.hidden = false;
    }
  }

  validaDatos() {

    var correcto = true;

    if (this.campoVacio(this.datos.nombre)){

      var elemento = this.$.nombre;

      elemento.focus();

      correcto = false;
    }

    if (this.campoVacio(this.datos.apellido)){

      var elemento = this.$.apellido;

      elemento.focus();

      correcto = false;
    }

    if (this.campoVacio(this.datos.codpostal)){

      var elemento = this.$.codpostal;

      elemento.focus();

      correcto = false;
    }

    if (this.campoVacio(this.datos.nif)){

      var elemento = this.$.nif;

      elemento.focus();

      correcto = false;
    }

    if (this.campoVacio(this.datos.telefono)){

      var elemento = this.$.telefono;

      elemento.focus();

      correcto = false;
    }

    if (!correcto){

      var elemento = this.$.errormodifica;

      elemento.textContent = "ERROR: Campo Obligatorio sin Informar";

      elemento = this.$.cajaerror;

      elemento.style.color = "black";
    }

    return correcto;
  }

  campoVacio(datoEntrada) {

      var retorno = true;

  		if (datoEntrada.length > 0 && datoEntrada != "")
  		{
  			for (var i = 0; i < datoEntrada.length; i++)
  			{
  				if (datoEntrada[i] != " ")
  				{
  					retorno = false;
  				}
  			}
  		}

  		return retorno;
	}

  manageAJAXresponse(data) {

    if (data.detail.response.msg == "Modificación correcta") {

        this.generaEvento("modificaUsuario");
    }
    else {
      var elemento = this.$.errormodifica;

      elemento.textContent = "ERROR" + data.detail.response.msg;

      elemento = this.$.cajaerror;

      elemento.style.color = "black";
    }

    var elemento = this.$.spinner;

    elemento.hidden = true;
  }

  showError(error) {

    var elemento = this.$.errorModifica;

    if (error.detail.request.status == 404){

      elemento.textContent = "ERROR: Usuario incorrecto";
    }
    else {
      elemento.textContent = error.detail.error.message;
    }

    elemento = this.$.spinner;

    elemento. hidden = true;

    elemento = this.$.cajaerror;

    elemento.style.color = "black";
  }

  generaEvento(evento) {

    this.dispatchEvent(
      new CustomEvent(
        "eventmodifica",
        {
          detail: {
            "evento": evento,
            "idUsuario" : this.idUsuario,
            "email": this.email,
            "datos": this.datos
          }
        }
      )
    )
  }
}

window.customElements.define('modifica-usuario', ModificaUsuario);
